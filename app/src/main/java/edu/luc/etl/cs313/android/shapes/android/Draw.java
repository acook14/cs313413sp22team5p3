package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

    // TO.DO entirely your job (except onCircle)

    private final Canvas canvas;

    private final Paint paint;

    public Draw(final Canvas canvas, final Paint paint) {
        this.canvas = canvas; // FIX.ME
        this.paint = paint; // FIX.ME
        paint.setStyle(Style.STROKE);
    }

    @Override
    public Void onCircle(final Circle c) {
        canvas.drawCircle(0, 0, c.getRadius(), paint);
        return null;
    }

    @Override
    public Void onStrokeColor(final StrokeColor c) {
        int temp = paint.getColor(); // Preserves 'default' color
        paint.setColor(c.getColor()); // Set color given by StrokeColor
        c.getShape().accept(this); // Draw shape tied to StrokeColor
        paint.setColor(temp); // Return to 'default' color
        return null;
    }

    @Override
    public Void onFill(final Fill f) {
        Style temp = paint.getStyle(); // Preserves 'default' Style
        paint.setStyle(Style.FILL_AND_STROKE);
        f.getShape().accept(this); // Draw shape tied to Fill
        paint.setStyle(temp); // Return to 'default' style
        return null;
    }

    @Override
    public Void onGroup(final Group g) {
        for (final Shape s : g.getShapes())
            s.accept(this); // Draw all shapes in the Group
        return null;
    }

    @Override
    public Void onLocation(final Location l) {
        canvas.translate(l.getX(), l.getY()); // Move to Location's given coordinates
        l.getShape().accept(this); // Draw shape tied to Location
        canvas.translate(-l.getX(), -l.getY()); // Return to (0,0)
        return null;
    }

    @Override
    public Void onRectangle(final Rectangle r) {
        canvas.drawRect(0,0, r.getWidth(), r.getHeight(), paint);
        return null;
    }

    @Override
    public Void onOutline(Outline o) {
        Style temp = paint.getStyle(); // Preserves 'default' style
        paint.setStyle(Style.STROKE);
        o.getShape().accept(this); // Draw shape tied to Outline
        paint.setStyle(temp); // Return to 'default' style
        return null;
    }

    @Override
    public Void onPolygon(final Polygon s) {

        // For n lines, need 4*n float value for the coordinates of line's endpoints
        final float[] pts = new float[4 * s.getPoints().size()];
        int length = pts.length;
        // Set the starting points
        pts[0] = s.getPoints().get(0).getX();
        pts[1] = s.getPoints().get(0).getY();
        int indexArray = 2;
        int indexPoints = 1;
        while (indexArray < length-2) {
            pts[indexArray++] = s.getPoints().get(indexPoints).getX();
            pts[indexArray++] = s.getPoints().get(indexPoints).getY();
            pts[indexArray++] = s.getPoints().get(indexPoints).getX();
            pts[indexArray++] = s.getPoints().get(indexPoints).getY();
            indexPoints++;
        }
        // Set the ending points (same as starting points to close polygon)
        pts[length-2] = s.getPoints().get(0).getX();
        pts[length-1] = s.getPoints().get(0).getY();
        canvas.drawLines(pts, paint);
        return null;
    }
}
