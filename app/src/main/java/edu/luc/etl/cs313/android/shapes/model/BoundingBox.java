package edu.luc.etl.cs313.android.shapes.model;

import static edu.luc.etl.cs313.android.shapes.model.Fixtures.simpleGroup;
import static java.lang.Math.*;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

    // TO.DO entirely your job (except onCircle)

    @Override
    public Location onCircle(final Circle c) {
        final int radius = c.getRadius();
        return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
    }

    @Override
    public Location onFill(final Fill f) {
        // Gives BoundingBox of the shape tied to Fill
        return f.getShape().accept(this);
    }

    @Override
    public Location onGroup(final Group g) {
        Location B1 = g.getShapes().get(0).accept(this);
        for(int i = 1; i < g.getShapes().size(); i++) {
            Location B2 = g.getShapes().get(i).accept(this);
            //Upper-Left
            int minX = min(B1.getX(), B2.getX());
            int minY = min(B1.getY(), B2.getY());
            // Bottom-Right
            int maxX = max(B1.getX() + ((Rectangle)B1.getShape()).getWidth(),
                    B2.getX() + ((Rectangle)B2.getShape()).getWidth());
            int maxY = max(B1.getY() + ((Rectangle)B1.getShape()).getHeight(),
                    B2.getY() + ((Rectangle)B2.getShape()).getHeight());

            int width = maxX - minX;
            int height = maxY - minY;
            B1 = new Location(minX, minY, new Rectangle(width, height));
        }
        return B1;
    }

    @Override
    public Location onLocation(final Location l) {
        // Gives BoundingBox of the shape tied to Location
        final Location bBox = l.getShape().accept(this);
        final int x = l.getX() + bBox.getX();
        final int y = l.getY() + bBox.getY();
        return new Location(x, y, bBox.getShape());
    }

    @Override
    public Location onRectangle(final Rectangle r) {
        return new Location(0, 0, new Rectangle(r.getWidth(), r.getHeight()));
    }

    @Override
    public Location onStrokeColor(final StrokeColor c) {
        // Gives BoundingBox of the shape tied to StrokeColor
        return c.getShape().accept(this);
    }

    @Override
    public Location onOutline(final Outline o) {
        // Gives BoundingBox of the shape tied to Outline
        return o.getShape().accept(this);
    }

    @Override
    public Location onPolygon(final Polygon s) {
        return onGroup(s);
    }

}
